# Bee audio classification

## Getting started

### Installation
Clone the repository into the directory where you want to have the code
```shell
git clone https://Rinze-Pieter@bitbucket.org/Rinze-Pieter/thema11-beep-bremote.git
```

### Requirements
* Librosa
    + this is a package that parses audio files and makes it possible to extract some features from the given audio file
    + link to the website: https://librosa.github.io/librosa/
    + Installation:
```shell
pip install librosa
```


* Python-Weka-Wrapper3 
    + this package makes it possible to use the Weka algorithms and the Weka filters in python
    + link to the website: https://pypi.org/project/python-weka-wrapper3/
    + Installation:
```shell
pip install python-weka-wrapper3
```

* Audiosegment
    + This package can be used to normalize the give audio file and it also makes it possible to change the type of a given audio file
    + link to the website: https://pypi.org/project/audiosegment/
    + Installation:
```shell
pip install Audiosegment
```

\* it is recommended to work in a virtual environment

\** the data used for this project can be found on the website: https://zenodo.org/record/1321278#.XGGJJ5wo9hE

### Usage
```shell
Scripts/Hive.py -u <.wav or .mp3 file>
```
or 
```shell
Scripts/Hive.py -u <directory with .wav or .mp3 files>
```
if you want to test the code you can try this shell command
```shell
Scripts/Hive.py -u "Data/TestFiles/"
```

and for help

```shell
Scripts/Hive.py -h
```

options for the Hive script

|options      | function                                                                                                                               | required | default          |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------|----------|------------------|
|-d, --data   | this is where you put the directory for the test and train dataset                                                                     | no       |                  |
|-u, --unknown| this is where you put the file or directory that you want to have classified                                                           | yes      |                  |
|-c, --cutoff | this is the cutoff value that is being used to cut  the file in small segments, these segments will also be as long as the cutoff value| no       | 2                |
|-o, --output | this is where you put the output file                                                                                                  | no       | output_labels.txt|



## Included files
* __Docs\\__
    + __ClassificationResults.pdf__ - The test results of the selection of machine learning algorithms that we wanted to try
    + __Wholeset_Results.zip__ - a zip file with all the results from the PCA on all the sound features
    + __Using bee audio to classify hive health.docx/.pdf__ - The research paper of this project

* __Data\\__
    + __TestFiles\\__ - this is a directory that can be used to check if the installation worked
    + __Test_Dataset.arff__ - this .arff file contains all the data for the test data set
    + __Train_Dataset.arff__ - this .arff file contains all the data for the train data set
    + __ROC_Normalized.arff__ - this .arff file contains all the data for the ROC_curve made in the ROC_Script.R
    
* __Scripts\\__
    + __Hive.py__ - This is the central code that is used to classify the bee sounds that u want to classify
    + __Weka_classifier.py__ - This is a class that makes the python-weka-wrapper3 API easier to use 
    + __Wav_Process_V2.py__ - This is a class that can be used to parse audio files and extract the mfcc and mel from them
    + __Arff_writer.py__ - this code is used to turn the features gotten from the audio files into one .arff file
    + __ROC_Script.R__ - this is a code snippet that is used to make a prettier ROC graph than the one that the python-weka-wrapper3 gives

## Known bugs
 * An ignored error:
 
    Exception ignored in: 
    
    <function WeakValueDictionary.\_\_init__.\<locals\>.remove at 0x7fe68c8be18>
    
    Traceback (most recent call last):
    
    file "/homes/rjonker/thema11-beep-bremote/Weka/lib/python3.5/weakref.py", line 117, in remove
    
    TypeError: 'NoneType' object is not callable
    
    This issue can be ignored, it changes nothing in the output. This can an issue due to our virtual environment

## Authors
* __[R. Jonker](https://bitbucket.org/%7Bc74c81b3-fb4f-42e0-b9a5-e531521e9f77%7D/)__ (ri.jonker@st.hanze.nl)
* __[W.J. Schuiten](https://bitbucket.org/%7Ba5962403-053f-40b4-ba67-399aad4de56c%7D/)__ (w.j.schuiten@st.hanze.nl)
