#!/usr/bin/env python3
"""
This script will try to classify all the beesounds given or all the beesounds in a given directory

usage:
    Weka_classifier.py -u <unknown file or directory> -o <output_path>
"""

# METADATA
__author__ = "Rinze-Pieter Jonker"
__status__ = "Working"
__version__ = "0.4"

# IMPORTS
import timeit
import os

import numpy as np
import weka.core.jvm as jvm
import weka.core.serialization as serialization

from weka.classifiers import Classifier, Evaluation
from weka.core.converters import Loader
from weka.core.dataset import Instance
from Wav_Process_V2 import WavProcess


# CODE
class WekaClassifier:
    def __init__(self, unknown, unknown_lab=None, evaluation=False, overwrite=False, cutoff=2, modelname="test_Model.model"):
        """
        This function initializes the classifier

        :param unknown: the path to the file you want to classify
        :param unknown_lab: the path to the lab file for the labels to the unknown file
        :param evaluation: a boolean that decides if you want to evaluate the algorithm
        :param overwrite: a boolean that decides if you want to overwrite the model and create a new one
        :param cutoff: an int which is the length of the segments in seconds
        :param modelname: the path to the model
        """
        self.unknown = unknown
        self.unknown_lab = unknown_lab
        self.evaluation = evaluation
        self.overwrite = overwrite
        self.cutoff = cutoff
        self.modelname = modelname
        self.classifier = "weka.classifiers.functions.SimpleLogistic"
        self.train_file = "../Data/Train_Dataset.arff"
        self.test_file = "../Data/Test_Dataset.arff"

    def create_classifier(self, loader):
        """
        this function creates the classifier used for this project if there isn't allready a .model file at
        the given location

        :param loader: a Weka Loader object, this is used to load in the .arff files
        :return: a Classifier object, a Instances object used for training, a Instances object used for testing
        """
        data = loader.load_file(self.train_file)
        test = loader.load_file(self.test_file)
        data.class_is_first()
        test.class_is_first()

        classifier = Classifier(classname=self.classifier)
        # classifier.options = ["-B", "50"]

        classifier.build_classifier(data)
        return classifier, data, test

    @staticmethod
    def evaluate_classifier(classifier, data, test):
        """
        This function evaluates the given classifier

        :param classifier: a Weka Classifier object
        :param data: a Weka Instances object that the classifier has been trained on
        :param test: a Weka Instances object that can be used as a test set to validate the classifier
        :return:
        """
        evl = Evaluation(data)
        evl.test_model(classifier, test)
        print(evl.summary())

    def parse_file(self):
        """
        This function will parse the unknown file into instances object so that the wav file can be classified

        :param unknown_file: the path to the Wav file that you want to test
        :param cutoff: an Int that is the length the files wil be cut in
        :return: a list with all the instance objects that need to be labeled
        """
        results = []
        # normalize=True so that the whole file becomes normalized
        wav_process = WavProcess(wavpath=self.unknown, normalize=True)

        if wav_process.get_length() > self.cutoff:
            # if the file is longer than the cutoff value the file needs to be sliced in to separate objects and they
            # will be appended to the results list
            end = 0
            while end < wav_process.get_length():
                begin = end
                end += self.cutoff

                libro = wav_process.segment_libro(begin, end)

                # The new WavProcess doesn't have to be normalized because the data gotten from the parent file
                # is allready normalized
                new_wav_process = WavProcess(libro=libro)

                features = new_wav_process.get_features()

                # parsing the results and adding them to a results list
                result = np.array(0)
                result = np.append(result, features[0])
                result = np.append(result, features[1])

                res_instance = Instance.create_instance(result)

                results.append(res_instance)
        else:
            # if the file is shorter that the cutoff value the file can just be used to parse al the data from it
            features = wav_process.get_features()
            result = np.array(0)
            result = np.append(result, features[0])
            result = np.append(result, features[1])

            res_instance = Instance.create_instance(result)

            results.append(res_instance)

        return results

    @staticmethod
    def classify_unknown(unknown_data, classifier, data):
        """
        This file wil classify all the data using the classifier given

        :param unknown_data: A list with Instance objects that need to be classified
        :param classifier: The classifier used to classify the unknown_data
        :param data: An Instances object that has been used to build the classifier
        :return: An Int with the label and an Int with the class distribution
        """
        label = 0
        mean_dist = 0

        for c, inst in enumerate(unknown_data, 1):
            inst.dataset = data
            prediction = classifier.classify_instance(inst)
            dist = classifier.distribution_for_instance(inst)
            label += prediction
            mean_dist += dist

        if not label == 0:
            label = label / len(unknown_data)
            mean_dist = mean_dist / len(unknown_data)

        return label, mean_dist

    def run(self):
        """
        this will run everything in the class
        :return: a list with the label of the file and the class distribution of the file
        """
        loader = Loader(classname="weka.core.converters.ArffLoader")

        if os.path.exists(self.modelname) and not self.overwrite:
            objects = serialization.read_all(self.modelname)
            classifier = Classifier(jobject=objects[0])
            data = loader.load_file(self.train_file)
            test = loader.load_file(self.test_file)
            data.class_is_first()
            test.class_is_first()

        else:
            if self.overwrite:
                print("> Overwriting current model at location '{}'".format(self.modelname))
                os.remove(self.modelname)
            else:
                print("> Creating new model ({})".format(self.modelname))
            classifier, data, test = self.create_classifier(loader)
            serialization.write_all(self.modelname, [classifier])

        if self.evaluation:
            self.evaluate_classifier(classifier, data, test)

        unknown_data = self.parse_file()

        information = self.classify_unknown(unknown_data, classifier, data)

        return information

    def __str__(self):
        return "> WekaClassifier object with '{}' at '{}' algorithm with file '{}'".format(self.classifier,
                                                                                           self.modelname,
                                                                                           self.unknown)


def main():
    evaluation = False  # optional, False as default
    override = False  # optional, False as default
    cutoff = 2  # optional, 2 as default
    filename = "Bee_Model.model"

    unknown_file = "../Data/TestFiles/No_Queen-Audio1.wav"  # required
    # unknown_file = "../Data/bee1_192_168_4_6-2017-08-01_11-45-01.wav"
    # unknown_file = "../Data/Hive1_12_06_2018_QueenBee_H1_audio___16_30_00_(4-6-bee).wav"
    # unknown_file = "../Data/Hive1_31_05_2018_NO_QueenBee_H1_audio___15_00_00_(0-2-bee).wav"

    classifier1 = WekaClassifier(unknown=unknown_file, modelname="../Data/Bee_Classification.model", evaluation=True)

    print(classifier1)

    label, mean_dist = classifier1.run()

    print("> label for file '{}' is {}".format(unknown_file, label))
    print("> class distribution for file '{}' is {}".format(unknown_file, mean_dist))
    return 0


if __name__ == "__main__":
    start = timeit.default_timer()

    jvm.start()
    main()
    jvm.stop()

    stop = timeit.default_timer()
    timer = stop - start

    print("> Running time = {} secs".format(round(timer, 2)))
