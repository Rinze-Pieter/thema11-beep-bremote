#!/usr/bin/env python3
"""
zo veel mogelijk kleine bestanden

bugs;
Exception ignored in: <function WeakValueDictionary.__init__.<locals>.remove at 0x7fe68c8be18>
Traceback (most recent call last):
    file "/homes/rjonker/thema11-beep-bremote/Weka/lib/python3.5/weakref.py", line 117, in remove
TypeError: 'NoneType' object is not callable
> Deze is gewoon te negeren, past de code niet aan
"""
# METADATA
__author__ = "Rinze-Pieter Jonker & Wouter Schuiten"
__status__ = "Working"
__version__ = "1.0"

# IMPORTS
import argparse
import glob
import os
import random
import timeit
import sys

import weka.core.jvm as jvm

from Weka_classifier import WekaClassifier
from Arff_writer import write_arff
from Wav_Process_V2 import WavProcess


# CODE
def argparser():
    """
    this function parses the commandline and looks for the right commands
    :return: a list with all the arguments
    """
    parse = argparse.ArgumentParser()
    parse.add_argument("-d", "--data",
                       help="This is where you put the path to the directory with all the bee sounds, this is optional",
                       required=False,
                       type=str)
    parse.add_argument("-u", "--unknown",
                       help="This is where you put the path to a directory or file that you want to label",
                       required=True)
    parse.add_argument("-c", "--cutoff",
                       help="This is where you put how long you want the segments to be" +
                            "and when you want to cut the files",
                       required=False,
                       default=2,
                       type=int)
    parse.add_argument("-o", "--output",
                       help="This is where you put the outputfile, if none is given this will be output_labels.txt",
                       required=False,
                       default="../output_labels.txt")
    args = parse.parse_args()
    return args.unknown, args.data, args.cutoff, args.output


def list_in_list(list1, list2):
    """
    This function will check if a list is inside of an other list

    :return: a boolean
    """
    in_list = False
    counter = 0
    while not in_list and counter < len(list1):
        in_list = all(x in list1[counter] for x in list2)
        counter = counter + 1
    return in_list


def create_files(arguments):
    """
    This function wil split the given directory into two data sets.
    Important for this function is that the filenames need to have the label for Queen or No_Queen in them

    :param arguments: the arguments given in the commandline
    :return: a list with all the data for the test file and a list wit all the data for the train file
    """
    file_dir_wav = glob.glob(arguments[1] + "*.wav")

    test_files = []
    train_files = []

    train = []
    test = []

    print("> Processing files, this might take a while ")

    # Separating the files for the test and train data set (10% chance to get into the test_files)
    for counter, file in enumerate(file_dir_wav):
        base_name = os.path.basename(file).strip(".wav")
        if "Queen" in base_name:
            if random.random() < 0.9:
                train_files.append(base_name)
            else:
                test_files.append(base_name)

    # Setting up the Progress bar
    toolbar_width = 15
    sys.stdout.write("> Parsing files, [%s]" % (" " * toolbar_width))
    sys.stdout.flush()
    sys.stdout.write("\b" * (toolbar_width + 1))

    # Parsing the files and returning the results into the test or train list
    for counter, file in enumerate(file_dir_wav):
        base_name = os.path.basename(file).strip(".wav")
        if "Queen" in base_name:
            sys.stdout.write("#")
            sys.stdout.flush()

            wav_process = WavProcess(wavpath=file, normalize=True)

            wav_length = wav_process.length
            seconds = 0

            if wav_process.length > arguments[2]:
                for item in range(round(wav_length / arguments[2])):
                    libro = wav_process.segment_libro(begin=seconds, end=seconds + arguments[2])

                    temp_wav = WavProcess(libro=libro)
                    mel, mfcc = temp_wav.get_features()
                    if "NO_Queen" in base_name or "Missing Queen" in base_name:
                        label = "NoQueen"
                    else:
                        label = "Queen"

                    if base_name in test_files:
                        test.append([label, mel.tolist(), mfcc.tolist()])
                    else:
                        train.append([label, mel.tolist(), mfcc.tolist()])

                    seconds += arguments[2]

            else:
                mel, mfcc = wav_process.get_features()
                if "NO_Queen" in base_name or "Missing Queen" in base_name:
                    label = "NoQueen"
                else:
                    label = "Queen"

                if base_name in test_files:
                    test.append([label, mel.tolist(), mfcc.tolist()])
                else:
                    train.append([label, mel.tolist(), mfcc.tolist()])
    sys.stdout.write("] \n")

    return test, train


def main():
    arguments = argparser()

    # Hardcoded in the filenames because Weka_classifier.y needs these files
    train_name = "../Data/Train_Dataset.arff"
    test_name = "../Data/Test_Dataset.arff"

    # Checking if the files already exist
    if not os.path.exists(train_name) and os.path.exists(test_name) or not arguments[1] is None:
        if arguments[1] is None:
            print("> Writing files at location '{}' and '{}'".format(train_name, test_name))
        else:
            print("> Overwriting the files at location '{}' and '{}'".format(train_name, test_name))
        test, train = create_files(arguments)

        # Writing the new .arff files
        write_arff(test, test_name)
        write_arff(train, train_name)

    # creating the classifier and running it
    with open(arguments[3], "w") as out:
        if os.path.isdir(arguments[0]):
            print("> Running classifier of files in directory {}".format(arguments[0]))
            dir_files = glob.glob(arguments[0] + "*.wav")
            if len(dir_files) != 0:
                for file in dir_files:
                    cls = WekaClassifier(unknown=file, modelname="../Data/Bee_Classification.model")
                    label, mean_dist = cls.run()

                    file = os.path.basename(file)

                    label_name = "Queen" if label > 0.5 else "NoQueen"
                    print("   > Classification for the file '{}' is '{}' ({})".format(file, label_name, label))
                    out.write("{}, {}, {}\n".format(file, label_name, label))
            else:
                print("> Directory does not have .wav files, exiting code ")

        else:
            cls = WekaClassifier(unknown=arguments[0], modelname="../Data/Bee_Classification.model")
            label, mean_dist = cls.run()
            file = os.path.basename(arguments[0])

            label_name = "Queen" if label > 0.5 else "NoQueen"
            print("> Classification for the file '{}' is '{}' ({})".format(file, label_name, label))
            out.write("{}, {}, {}\n".format(file, label_name, label))

    return 0


if __name__ == "__main__":
    start = timeit.default_timer()

    jvm.start()
    main()
    jvm.stop()

    # try:
    #     jvm.start()
    #     main()
    # except:
    #     print("> Caught an error, exiting the code")
    # finally:
    #     jvm.stop()

    print("> Running time was {} secs".format(timeit.default_timer() - start))
