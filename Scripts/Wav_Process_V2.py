#!/usr/bin/env python 3
"""
This script will process a .wav or a .mp3 file and will create a WavProcess object with all some of the sound features
"""

# METADATA
__author__ = "Rinze-Pieter Jonker & Wouter Schuiten"
__status__ = "Working"
__version__ = "2.0"

# IMPORTS
import librosa
import glob
import sys
import os

import numpy as np

from pydub import AudioSegment


# CODE
class WavProcess:
    def __init__(self, wavpath=None, libro=None, normalize=False):
        self.normalize = normalize

        # Checking if the given data is good
        if wavpath is None and libro is None:
            print("> Add a wavpath or the sample rate and amp of a wavfile")
            print("> Exiting code")
            sys.exit()

        if wavpath is not None and "mp3" in wavpath:
            # Checking if the given file is a mp3 file
            self.wavpath = self.mp3_to_wav(wavpath=wavpath)
        else:
            self.wavpath = wavpath

        if libro is None:
            libro, sample_rate = self.get_amp()
            self.libro = libro
            self.sample_rate = sample_rate
        else:
            self.libro = libro[0]
            self.sample_rate = libro[1]
        self.length = self.get_length()

    @staticmethod
    def mp3_to_wav(wavpath):
        """
        This function will transform the given file from mp3 format to wav format

        :return: the path to the wav file
        """
        # Changing the output name to a .wav file
        output_file = wavpath.strip("mp3") + "wav"
        sound = AudioSegment.from_mp3(wavpath)
        sound.export(output_file, format="wav")
        return output_file

    def get_amp(self):
        """
        This function will return all the data from the librosa package

        :return: a list with the amp of the file and the sample rate (Int)
        """
        # Checking if the file must be normalized
        if self.normalize:
            old_wav = self.wavpath
            self.normalize_audio()

        libro, sample_rate = librosa.load(self.wavpath)

        # Removing the normalized file if it was made
        if self.normalize:
            self.delete_file()
            self.wavpath = old_wav
        return libro, sample_rate

    def get_features(self):
        """
        This function will extract all the features needed for the classification

        :return: a list with all the mels and a list with all the mfccs
        """
        mel = np.mean(librosa.feature.melspectrogram(self.libro, sr=self.sample_rate, fmax=700).T, axis=0)[25:]
        mfccs = librosa.feature.mfcc(S=librosa.power_to_db(mel[-100:]).T, axis=0)[:12]
        return mel, mfccs

    def segment_libro(self, begin, end):
        """
        This function will cut a piece of the soundfile and will return the amplitude and the sample rate for that
        part of the file

        :param begin: the second the fragment needs to begin
        :param end: the second the fragment needs to end
        :return: the amplitude and sample rate of that fragment
        """
        begin_frame = round(self.sample_rate * begin)
        end_frame = round(self.sample_rate * end)
        return self.libro[begin_frame:end_frame], self.sample_rate

    def normalize_audio(self):
        """
        This function will make a normalized wav file and will set the path to this wav file as the wavpath
        """
        sound = AudioSegment.from_file(self.wavpath)
        normfile = sound.apply_gain(-20 - sound.dBFS)

        normfile_name = "{}_norm.wav".format(self.wavpath.strip(".wav"))

        normfile.export(normfile_name, format="wav")
        self.wavpath = normfile_name

    def delete_file(self):
        """
        this function will delete the file at the given wavpath
        """
        os.remove(self.wavpath)

    def get_length(self):
        """
        This function will return the length of the file in seconds

        :return: Amount of seconds (Int)
        """
        return round(len(self.libro) / self.sample_rate)

    def __str__(self):
        return"> WavProcess object with: \n> wavpath = {} \n> sample rate = {}\n> length = {}".format(self.wavpath,
                                                                                                      self.sample_rate,
                                                                                                      self.length)


def create_lab_dict(directory):
    """
    This function will search for lab files with labels in them in the given directory and create a dictionary with the
    data from the labfile as value and the file name as key, so that this directory can be used in tandem with the .wav
    files

    :param directory: the directory where the files come from
    :return: a dictionary with the bee, no_bee information
    """
    input_dir = glob.glob("{}*.lab".format(directory))
    result_dict = {}
    for file in input_dir:
        with open(file, "r") as f:
            filename = file.strip(directory).strip(".lab")
            bee = []
            no_bee = []
            for count, item in enumerate(f):
                if not count == 0:
                    item_striped = item.strip("\n").split("\t")
                    if not item_striped[0] == ".":
                        item_striped[0] = round(float(item_striped[0]))
                        item_striped[1] = round(float(item_striped[1]))
                        item_range = list(range(item_striped[0], item_striped[1]))
                        if item_striped[2] == "bee":
                            bee.append(item_range)
                        else:
                            no_bee.append(item_range)
            result_dict[filename] = (bee, no_bee)
    return result_dict


def main():
    """
    the main script of the funcion, this will test some functions
    :return: 0
    """
    wav_process = WavProcess(wavpath="../Data/TestFiles/No_Queen-Audio1.wav")
    print(wav_process)
    return 0


if __name__ == "__main__":
    main()
